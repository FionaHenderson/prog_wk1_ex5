﻿using System;

namespace Exercise5
{
    
    class MainClass
    {
        static void Main(string[] args)
        {
            var a = 8;
            var b = 13;
            var answer = a + b;

            //I chose this method to reference the variables & var answer can be used again.
            Console.WriteLine(answer);
            Console.WriteLine($"The answer to {a} plus {b} is {answer}");
        }
    }
}
